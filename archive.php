<?php
get_header(); // This fxn gets the header.php file and renders it
?>

<?php
if( !is_page(9) ){
?>

<div class="rows ovf-hi gallery_img_3"> 
  <!--one block-->
  <div class="one-block banner-img"><img src="<?php
          $image = get_field('bannerpage');
		  if( !empty($image) ){
			  echo $image['url'];
		  }else{
			  echo get_template_directory_uri()."/img/web/default-banner.jpg";
			 }
		 ?>" alt="banner"> </div>
</div>
<!---/gallery_img_3-->

<?php } ?>
<!--Content visual-->
    <?php if ( have_posts() ) : ?>
    
      <?php while(have_posts()) : the_post();?>
      
                <div class="col3">

                  <a  href="<?php the_permalink() ?>">
                    <img src="<?php the_field('product_image'); ?><?php the_field('product_image'); ?>" alt="<?php the_title(); ?>" width="300" height="300" class="alignnone size-medium wp-image-165">
                    <p class="product_name"><?php the_title(); ?></p>
                  </a>

                   </div>
      
       <?php endwhile; ?>
    
    <?php
		endif;
		?>   	  
<!--End Content visual-->

<?php get_footer(); // This fxn gets the footer.php file and renders it ?>