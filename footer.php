<footer>
	<div class="n-footer mgt-60">
        <div class="cont">
            <div class="n-ft-logo hv-o"><a href="<?php echo get_site_url(); ?>"><img src="<?php the_field('logo_footer','option'); ?>" alt="海外・国内リゾート不動産　取扱サイト"></a></div>
            <div class="n-ft-menu">
            	<ul>
                	<li><a href="<?php echo get_site_url(); ?>">ホーム</a></li>
                	<li><a href="<?php echo get_site_url(); ?>/?page_id=7">会員登録ページ</a></li>
                	<li><a href="<?php echo get_site_url(); ?>/?page_id=11">会員掲載ホーム</a></li>
                	<li><a href="<?php echo get_site_url(); ?>/?page_id=13">地域情報</a></li>
                	<li><a href="<?php echo get_site_url(); ?>/?page_id=15">ランキング</a></li>
                	<li><a href="<?php echo get_site_url(); ?>/?page_id=17#company">会社概要</a></li>
                	<li><a href="<?php echo get_site_url(); ?>/?page_id=17">お問い合わせ</a></li>
                </ul>
                <div class="n-copyright mgt-5">Copyright © 海外・国内リゾート不動産 取扱サイト. All Rights Reserved. <a href="http://chaco-web.com" target="_blank">Web design CHACOWEB.COM</a></div>
            </div>
        </div>
    </div>
</footer>

<!-- end html --> 

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.bxslider.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script> 
<!-- end scripts -->

<script>
$(document).ready(function() {
	
    $('#owlslider').owlCarousel({
				loop:true, // loop all item
				center : true, // Center item. Works well with even an odd number of items.
				autoplay : true,
				slideSpeed : 100,
				autoplaySpeed : 500, // 
				navSpeed : 500, // Navigation speed when click button.
				autoplayHoverPause : true, //Pause on mouse hover.
				nav:false, // Show next/prev buttons.
				navText : ["<img src='assets/images/btn-previous.png'>","<img src='assets/images/btn-next.png'>"],
				dots : true, // Show dots navigation.
				pagination : true,
				paginationNumbers : true,
				animateOut: 'fadeOut',
				items : 1, // single item
		}); // owl slide
});
</script> 


</body>
</html>

<?php wp_footer(); ?>