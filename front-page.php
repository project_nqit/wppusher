<?php get_header();?>

<section id="content">
	<div class="n-main mgt-50">
        <div class="cont">
        	<div class="n-left">
                
                <table class="in-table">
					<tr>
					<?php
                    // check if the repeater field has rows of data
                    if( have_rows('index_photo','option') ):
                        // loop through the rows of data
                        $i=1;
                        while ( have_rows('index_photo','option') ) : the_row();?>
                        
							<?php if($i==1) { ?><td rowspan="2" colspan="2"><img src="<?php the_sub_field('index_photo_img','option'); ?>" alt="Sea Side Life"></td><?php } else if($i==2) { ?><td><img src="<?php the_sub_field('index_photo_img','option'); ?>" alt="Sea Side Life"></td></tr><tr><?php } else if($i==3) { ?>
                            <td><img src="<?php the_sub_field('index_photo_img','option'); ?>" alt="Sea Side Life"></td></tr><tr>
                            <?php } else { ?>
                            <td><img src="<?php the_sub_field('index_photo_img','option'); ?>" alt="Sea Side Life"></td>
                            <?php } ?>
                            <?php if($i%3==0) { echo '</tr><tr>'; }?>
                        <?php $i++; endwhile;
                    else :
                        // no rows found
                    endif;
                    ?>
                
                </table>
                
                <div class="in-map mgt-60"><img src="http://files.moo.jp/8120161214kita/wp-content/themes/gkv/assets/images/index/map.png" alt="Sea Side Life" /></div>
                
				<?php if ( have_posts() ) :  ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                            <?php the_content();  ?>
                             <?php endwhile; ?>
                    <?php  wp_reset_query(); ?>
                <?php  endif; ?>
                
            </div><!--left-->
            
            <?php include("sidebar.php"); ?>
	    </div>
    </div>
</section>

<?php get_footer(); ?>