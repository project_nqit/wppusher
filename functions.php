<?php
add_theme_support( 'post-thumbnails' );
add_theme_support( 'widgets' );
add_theme_support( 'menus' );

//include('includes/realestate.php'); 
include('includes/theme-options.php'); 
include('includes/widget.php'); 


//include('includes/permalink.php');
include('includes/disable-menu-admin.php');

add_filter( 'auto_update_plugin', '__return_false' ); // disable update plugin
// define( 'AUTOMATIC_UPDATER_DISABLED', true ); // if u want disable all, paste this code to wp-config
function admin_style() {wp_enqueue_style('admin-styles', get_template_directory_uri().'/includes/admin.css');} add_action('admin_enqueue_scripts', 'admin_style');
function my_login_stylesheet() {  wp_enqueue_style('admin-styles', get_template_directory_uri().'/includes/admin.css');} add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );