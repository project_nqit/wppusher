<title><?php if( get_field('title','option') ){ the_field('title','option'); }else{ bloginfo('name'); echo " | "; is_front_page() ? bloginfo('description') : wp_title('');} ?></title>
<meta name="keywords" content="<?php if( get_field('keyword','option') ){ the_field('keyword','option'); }else{echo "***";} ?>" >
<meta name="description" content="<?php if( get_field('description','option') ){ the_field('description','option'); }else{echo "***"; }?>">

<?php echo get_template_directory_uri() ?>/

<?php echo get_home_url(); ?>/?page_id=1

<?php wp_head(); 