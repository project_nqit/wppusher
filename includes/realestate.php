<?php
// Register Custom post type
add_action( 'init', 'register_my_cpts' );
function register_my_cpts() {
	$labels = array(
		"name" => "Realestates",
		"singular_name" => "Realestate",
		"menu_name" => "Realestates",
		"all_items" => "All Realestates",
		"add_new" => "Add New",
		"add_new_item" => "Add new Realestate",
		"edit" => "Edit",
		"edit_item" => "Edit Realestate",
		"new_item" => "New Realestate",
		"view" => "View",
		"view_item" => "View Realestate",
		"search_items" => "Search Realestate",
		"not_found" => "No Realestates found",
		"not_found_in_trash" => "No Realestates found in Trash",
		"parent" => "Parent Realestate",
		);
	$args = array(
		"labels" => $labels,
		"description" => "Custom post type Realestate",
		"public" => true,
		"show_ui" => true,
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "realestate", "with_front" => true ),
        "supports" => array( "title", "editor", "author", "thumbnail", "excerpt"),
		"query_var" => true,
		"menu_position" => 4,"menu_icon" => "dashicons-welcome-add-page",				
		"taxonomies" => array( "tags_realestate", "categories_realestate" )
	);
	register_post_type( "realestate", $args );
// End of register_my_cpts()
}
 
// Register Taxonomies
add_action( 'init', 'register_my_taxes' );
function register_my_taxes() {
	$labels = array(
		"name" => "Categories Realestates",
		"label" => "Categories Realestates",
		"menu_name" => "Categories",
		"all_items" => "All Categories",
		"edit_item" => "Edit Category",
		"view_item" => "View Category",
		"update_item" => "Update Category Name",
		"add_new_item" => "Add New Category",
		"new_item_name" => "New Category Name",
		"parent_item" => "Partent Category",
		"parent_item_colon" => "Partent Category:",
		"search_items" => "Search Categories",
		"popular_items" => "Popular Categories",
		"separate_items_with_commas" => "Separate categories with commas",
		"add_or_remove_items" => "Add or remove categories",
		"choose_from_most_used" => "Choose from the most used categories",
		"not_found" => "No categories  found",
		);
	$args = array(
		"labels" => $labels,
		"hierarchical" => true,
		"label" => "Categories Realestates",
		"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'categories_realestate', 'with_front' => true ),
		"show_admin_column" => false,
	);
	register_taxonomy( "categories_realestate", array( "realestate" ), $args );
	
	$labels = array(
		"name" => "Tags Realestates",
		"label" => "Tags Realestates",
		"menu_name" => "Tags",
		"all_items" => "All Tags",
		"edit_item" => "Edit Tag",
		"view_item" => "View Tag",
		"update_item" => "Update Tag Name",
		"add_new_item" => "Add New Tag",
		"new_item_name" => "New Tag Name",
		"parent_item" => "Parent Tag",
		"parent_item_colon" => "Parent Tag:",
		"search_items" => "Search Tags",
		"popular_items" => "Popular Tags",
		"separate_items_with_commas" => "Separate tags with commas",
		"add_or_remove_items" => "Add or remove tags",
		"choose_from_most_used" => "Choose from the most used tags",
		"not_found" => "No tags found",
		);
	$args = array(
		"labels" => $labels,
		"hierarchical" => false,
		"label" => "Tags Realestates",
		"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'tags_realestate', 'with_front' => true ),
		"show_admin_column" => false,
	);
	register_taxonomy( "tags_realestate", array( "realestate" ), $args );
// End register_my_taxes
}
?>