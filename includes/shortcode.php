<?php
function upload_url_f($args,$content){
	$link = wp_upload_dir();
	return $link['baseurl'];
}
add_shortcode('upload_url', 'upload_url_f');

function img_url_f($args,$content){
	$link = get_template_directory_uri();
	return $link;
}
add_shortcode('img_url', 'img_url_f');

function site_url_f($args,$content){
	$link = get_site_url();
	return $link;
}
add_shortcode('site_url', 'site_url_f');
