<?php
get_header(); // This fxn gets the header.php file and renders it
 ?>


    <!--Content visual-->
	
      <?php if ( have_posts() ) : 
			// Do we have any posts/pages in the databse that match our query?
	  ?>
	  
	       <?php while ( have_posts() ) : the_post(); 
				// If we have a page to show, start a loop that will display it
				?>
				
					<?php the_content(); 
							// This call the main content of the page, the stuff in the main text box while composing.
							// This will wrap everything in p tags
							?>
				
		<?php  endwhile; ?>		
	  
	  <?php  endif; ?>
	  
<!--End Content visual-->

<?php get_footer(); // This fxn gets the footer.php file and renders it ?>