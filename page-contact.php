<?php /* Template Name: Contact */ ?>
<?php get_header();?>
 <!-- Content -->
 <section>
    <div class="wrap-slide rows banner-common">
      <div class="grid-1192">
        <div class="banner-image rows">
        <?php if ( has_post_thumbnail() ) { ?>
            <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
            <?php } else { ?>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/banner1.jpg" alt="<?php the_title(); ?>">
        <?php } ?>
        </div>
      </div>
      <!-- end /.grid-1192-->
    </div>
    <!-- end /.banner-common-->
    <div class="breakum-page rows">
        <?php get_template_part( 'inc/breakum'); ?>
    </div><!-- end /.breakum-page-->
    <div class="content-page rows">
      <div class="p2-content rows">
        <div class="cont">
          <div class="page-rows rows">
            <div class="page-item rows">
              <h3 class="title-style-1">
                <?php the_field('title_contact_en'); ?>
                <span class="title-jp"><?php the_field('title_contact_jp'); ?></span>
              </h3>
              <div class="p5-content rows">
                <div class="flex-center">
                  <div class="p5-left">
                  <form method="post" id="s_contact_form" class="contact-form" name="s_contact_form" action="<?php echo get_template_directory_uri() ?>/s_sendmail.php">
                    <input type="hidden" name="check_spam" value="hsspammail_CV9ma2QkEQD886bLXp">
                        <div class="p5-item rows">
                            <div class="p5-item-title rows">
                                <strong>お問い合わせ内容</strong>
                                <span class="req">※必須</span>
                            </div>
                            <div class="p5-item-content rows">
                                <div class="radio-item rows">
                                <div class="radio">
                                    <input type="radio" name="お問い合わせ内容" id="radio1" value="お見積り" checked>
                                    <label for="radio1">
                                    お見積り
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" name="お問い合わせ内容" id="radio2" value="新築・リフォーム相談">
                                    <label for="radio2">
                                    新築・リフォーム相談
                                    </label>
                                </div>
                                </div>
                                <div class="radio-item rows">
                                <div class="radio">
                                    <input type="radio" name="お問い合わせ内容" id="radio3" value="各種建物に関する相談">
                                    <label for="radio3">
                                    各種建物に関する相談
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" name="お問い合わせ内容" id="radio4" value="その他">
                                    <label for="radio4">
                                    その他
                                    </label>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end /.p5-item-->
                            <div class="p5-item rows">
                            <div class="p5-item-title rows">
                                <strong>お名前〔漢字〕</strong>
                                <span class="req">※必須</span>
                            </div>
                            <div class="p5-item-content rows">
                                <div class="p5-item-g47">
                                姓
                                <input type="text" class="form-control dib w166" name="お名前姓(check)">
                                </div>
                                <div class="p5-item-g53">
                                名
                                <input type="text" class="form-control dib w243" name="お名前名(check)">
                                </div>
                            </div>
                            </div>
                            <!-- end /.p5-item-->
                            <div class="p5-item rows">
                            <div class="p5-item-title rows">
                                <strong>フリガナ〔全角カタカナ〕</strong>
                                <span class="req">※必須</span>
                            </div>
                            <div class="p5-item-content rows">
                                <div class="p5-item-g47">
                                姓
                                <input type="text" class="form-control dib w166" name="フリガナ姓(check)">
                                </div>
                                <div class="p5-item-g53">
                                名
                                <input type="text" class="form-control dib w243" name="フリガナ名(check)">
                                </div>
                            </div>
                            </div>
                            <!-- end /.p5-item-->
                            <div class="p5-item rows">
                            <div class="p5-item-title rows">
                                <strong>メールアドレス〔半角英数字〕</strong><span class="req">※必須</span>
                            </div>
                            <div class="p5-item-content rows">
                                <input type="text" class="form-control" name="s_email(check)">
                                <span class="note-input">（確認用）もう一度メールアドレスをご入力下さい。</span>
                                <input type="text" class="form-control" name="s_confirm_email(check)">
                            </div>
                            </div>
                            <!-- end /.p5-item-->
                            <div class="p5-item rows">
                            <div class="p5-item-title rows">
                                <strong>連絡先電話番号</strong>
                                <span class="req">※必須</span>
                                <span class="fz-15">
                                <strong>〔半角数字〕ご連絡のとれる電話番号をご入力下さい。</strong>
                                </span>
                            </div>
                            <div class="p5-item-content rows">
                                <input type="text" class="form-control dib w105" name="連絡先電話番号(check)" maxlength="3" pattern="[0-9]+" oninvalid="setCustomValidity('_連絡先電話番号')"  onchange="try{setCustomValidity('')}catch(e){}"> ―
                                <input type="text" class="form-control dib w105" name="連絡先電話番号2(check)" maxlength="3" pattern="[0-9]+" oninvalid="setCustomValidity('_連絡先電話番号')"  onchange="try{setCustomValidity('')}catch(e){}"> ―
                                <input type="text" class="form-control dib w105" name="連絡先電話番号3(check)" maxlength="4" pattern="[0-9]+" oninvalid="setCustomValidity('_連絡先電話番号')"  onchange="try{setCustomValidity('')}catch(e){}">
                                <span class="note-input"> 住所〔全角〕<span class="req-black">※任意</span></span>
                                <input type="text" class="form-control" name="住所(check)">
                            </div>
                            </div>
                            <!-- end /.p5-item-->
                            <div class="p5-item rows">
                            <div class="p5-item-title rows">
                                <strong>お問い合わせ内容</strong>
                                <span class="req">※必須</span>
                            </div>
                            <div class="p5-item-content rows">
                                <textarea class="form-control" name="お問い合わせ内容(check)"></textarea>
                            </div>
                            </div>
                            <!-- end /.p5-item-->
                            <div class="p5-btn rows">
                            <button class="btn-send" type="submit" value="送信">送信</button>
                            <button class="btn-res" type="reset">リセット</button>
                            </div>
                        </form>
                  </div>
                  <!-- end ./p5-left-->
                  <div class="p5-right">
                    <img src="<?php the_field('image_contact'); ?>"  alt="お問い合わせ">
                  </div>
                  <!-- end /.p5-right-->
                </div>
              </div>
            </div>
            <!-- end ./page-item-->
          </div>
          <!-- end /.page-rows-->
        </div>
        <!-- end /.cont-->
      </div>
      <!-- end /.p2-content-->
    </div>
  </section>
  <!-- End content -->
<?php get_footer(); ?>