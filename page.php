<?php get_header();?>
<div class="n-breakcum"><div class="cont">
    <a href="<?php echo get_site_url(); ?>">ホーム</a>
    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
    <?php the_title(); ?>
</div></div>
<section id="content">
    <div class="n-main">
		<div class="cont">
        	<div class="n-left">
				<?php if ( have_posts() ) :  ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                            <?php the_content();  ?>
                             <?php endwhile; ?>
                    <?php  wp_reset_query(); ?>
                <?php  endif; ?>
            </div><!--left-->

            <?php include("sidebar.php"); ?>
            
        </div>
    </div>
</section>

<?php get_footer(); ?>