<?php 
/**
 * 	This is the sidebar!
 *
 *	This file will render the sidebar, as defined by the user in the admin area
 *
*/
?>
<?php if(is_front_page()) { ?>
<div class="n-right">
	
    <?php
	// check if the repeater field has rows of data
	if( have_rows('sidebar_banner_2','option') ):
		// loop through the rows of data
		while ( have_rows('sidebar_banner_2','option') ) : the_row(); ?>

    <div class="sb-block hv-o mgb-12"><a href="<?php the_sub_field('sidebar_link2','option'); ?>"><img src="<?php the_sub_field('sidebar_photo2','option'); ?>" alt="Banner"></a></div>
        
    <?php	endwhile;
	else :
		// no rows found
	endif;
	?>
    
    <div class="sb-item mgt-3">
        <div class="sb-item-title"><i class="fa fa-play-circle" aria-hidden="true"></i> ランキング</div>
        <div class="sb-item-content">
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img1.png" alt="ランキング"></a></div>
                <div class="sb-item-text"><a href="#">テキストテキストテキストテキストテキストテキストテキストテキスト</a></div>
            </div><!---->
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img2.png" alt="ランキング"></a></div>
                <div class="sb-item-text"><a href="#">テキストテキストテキストテキストテキストテキストテキストテキスト</a></div>
            </div><!---->
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img3.png" alt="ランキング"></a></div>
                <div class="sb-item-text"><a href="#">テキストテキストテキストテキストテキストテキストテキストテキスト</a></div>
            </div><!---->
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img4.png" alt="ランキング"></a></div>
                <div class="sb-item-text"><a href="#">テキストテキストテキストテキストテキストテキストテキストテキスト</a></div>
            </div><!---->
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img5.png" alt="ランキング"></a></div>
                <div class="sb-item-text"><a href="#">テキストテキストテキストテキストテキストテキストテキストテキスト</a></div>
            </div><!---->
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img6.png" alt="ランキング"></div>
                <div class="sb-item-text">テキストテキストテキストテキストテキストテキストテキストテキスト</div>
            </div><!---->
        </div><!--content-->
    </div>
    
    <?php
	// check if the repeater field has rows of data
	if( have_rows('sidebar_banner','option') ):
		// loop through the rows of data
		while ( have_rows('sidebar_banner','option') ) : the_row(); ?>
					
    	
    <div class="sb-block hv-o mgt-12"><a href="<?php the_sub_field('sidebar_link','option'); ?>"><img src="<?php the_sub_field('sidebar_photo','option'); ?>" alt="Banner"></a></div>
        
    <?php	endwhile;
	else :
		// no rows found
	endif;
	?>
    
</div>

<?php } else { ?>

<div class="n-right">
    
    <div class="sb-item">
        <div class="sb-item-title"><i class="fa fa-play-circle" aria-hidden="true"></i> ランキング</div>
        <div class="sb-item-content">
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img1.png" alt="ランキング"></a></div>
                <div class="sb-item-text"><a href="#">テキストテキストテキストテキストテキストテキストテキストテキスト</a></div>
            </div><!---->
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img2.png" alt="ランキング"></a></div>
                <div class="sb-item-text"><a href="#">テキストテキストテキストテキストテキストテキストテキストテキスト</a></div>
            </div><!---->
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img3.png" alt="ランキング"></a></div>
                <div class="sb-item-text"><a href="#">テキストテキストテキストテキストテキストテキストテキストテキスト</a></div>
            </div><!---->
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img4.png" alt="ランキング"></a></div>
                <div class="sb-item-text"><a href="#">テキストテキストテキストテキストテキストテキストテキストテキスト</a></div>
            </div><!---->
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img5.png" alt="ランキング"></a></div>
                <div class="sb-item-text"><a href="#">テキストテキストテキストテキストテキストテキストテキストテキスト</a></div>
            </div><!---->
            <div class="sb-item-row">
                <div class="sb-item-photo hv-o"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/general/sb-img6.png" alt="ランキング"></div>
                <div class="sb-item-text">テキストテキストテキストテキストテキストテキストテキストテキスト</div>
            </div><!---->
        </div><!--content-->
    </div>
    
    <?php
	// check if the repeater field has rows of data
	if( have_rows('index_photo','option') ):
		// loop through the rows of data
		while ( have_rows('index_photo','option') ) : the_row(); ?>
    
    <div class="sb-block-right hv-o mgt-12" <?php if(get_sub_field('show_sidebar','option')==false) { echo 'style="display:none"'; } ?>><a href="#"><img src="<?php the_sub_field('index_photo_img','option'); ?>" alt="フォーム"></a></div>
    
    <?php	endwhile;
	else :
		// no rows found
	endif;
	?>
    
    <?php
	// check if the repeater field has rows of data
	if( have_rows('sidebar_banner_2','option') ):
		// loop through the rows of data
		while ( have_rows('sidebar_banner_2','option') ) : the_row(); ?>
        
    <div class="sb-block hv-o mgt-12"><a href="<?php the_sub_field('sidebar_link2','option'); ?>"><img src="<?php the_sub_field('sidebar_photo2','option'); ?>" alt="Banner"></a></div>
    
    <?php	endwhile;
	else :
		// no rows found
	endif;
	?>
    
</div><!--right-->
            
<div class="n-row mgt-40">   
	<?php
	// check if the repeater field has rows of data
	if( have_rows('sidebar_banner','option') ):
		// loop through the rows of data
		while ( have_rows('sidebar_banner','option') ) : the_row(); ?>
					
    	<div class="sb-block-ft hv-o mgt-12"><a href="<?php the_sub_field('sidebar_link','option'); ?>"><img src="<?php the_sub_field('sidebar_photo','option'); ?>" alt="Banner"></a></div>
    <?php	endwhile;
	else :
		// no rows found
	endif;
	?>
</div>

<?php } ?>