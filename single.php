<?php
get_header(); // This fxn gets the header.php file and renders it
 ?>


<?php

if ( have_posts() ){ ?>
	
	 <?php while ( have_posts() ) : the_post(); 
				// If we have a page to show, start a loop that will display it
	   ?>

<!--slide-->
<div class="se-bg-link-menu">
    <div class="se-link-menu">
      <a href="<?php echo get_site_url(); ?>" class="se-link se-active">ホーム</a>  > 
	  <span class="se-link"><?php the_title() ?></span>
</div>
</div>
<!--slide--> 

<!--content-->
<div id="g-bg-content">
   
   <div class="se-bg-ct-01">
        <div class="se-ct-01">
			<h2><span class="fz-35"><?php the_title() ?></span></h2>
			<h2><span class="fz-25">
			<?php 
			$custom_fields_post = get_post_custom(); 
			$small_title = $custom_fields_post['small_title'];
			      foreach ( $small_title as $key => $value ) {
                           echo $value;
                   }    
			?>
			</span></h2>
        </div>
    </div>
	
<!--Content visual-->
     
					<?php the_content(); 
							// This call the main content of the page, the stuff in the main text box while composing.
							// This will wrap everything in p tags
							?>
		
<!--End Content visual-->  

</div>

<!--end content--> 
	    
	   
	  <?php  endwhile; ?> 
	
<?php	}else{  ?>
	
	<article class="post-error">
					<h1 class="404">Nothing posted yet</h1>
	</article>
	
<?php   }   ?>


<?php get_footer(); // This fxn gets the footer.php file and renders it ?>